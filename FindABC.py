#!/usr/bin/python

import urllib, json, re, math

# A few of the things worth searching for...

#BTAC
#productID = "027096" # Sazerac Rye 18 Yr.
#productID = "017756" # Eagle Rare 17Yr
#productID = "018416" # George T Stagg
#productID = "022086" # William Laure Weller

#Van Winkle
#productID = "020140" # Van Winkle 10 Yr. (ORVW)
#productID = "021906" # Van Winkle 12 Yr.
#productID = "020150" # Pappy Van Winkle 15 Yr.
productID = "021016" # Pappy Van Winkle 20 Yr.

#Misc
#productID = "021540" # Stagg Jr.
#productID = "027484" # Abraham Bowman
#productID = "027100" # Sazerac Rye 6 Yr.
#productID = "019481" # Maker's Mark Cask Strength
#productID = "000811" # Old Forester Birthday Bourbon
#productID = "017914" # Elijah Craig Barrel Proof
#productID = "017946" # Elmer T Lee
#productID = "018326" # Forged Oak
#productID = "021279" # Rock Hill Farms

#Dickel
#productID = "026594" # Dickel 14
#productID = "026592" # Dickel 9

#E. H. Taylor
#productID = "021601" # EH Taylor Cured Oak
#productID = "021600" # EH Taylor Barrel Proof

#Jefferson's
#productID = "019012" # Jefferson's Presidential Select
#productID = "027053" # Jefferson's 21 Yr Rye
#productID = "019007" # Jefferson's 25 Yr Bourbon
#productID = "027055" # Jefferson's 25 Yr Rye
#productID = "019008" # Jefferson's 21 Yr Bourbon
#productID = "027052" # Jefferson's Rye
#productID = "024326" # Jefferson's Chef's Collaboration

#Irish Whiskey
#productID = "015606" # Green Spot

def distance_on_unit_sphere(lat1, long1, lat2, long2):
 
    # Convert latitude and longitude to 
    # spherical coordinates in radians.
    degrees_to_radians = math.pi/180.0
         
    # phi = 90 - latitude
    phi1 = (90.0 - lat1)*degrees_to_radians
    phi2 = (90.0 - lat2)*degrees_to_radians
         
    # theta = longitude
    theta1 = long1*degrees_to_radians
    theta2 = long2*degrees_to_radians
         
    # Compute spherical distance from spherical coordinates.
         
    # For two locations in spherical coordinates 
    # (1, theta, phi) and (1, theta, phi)
    # cosine( arc length ) = 
    #    sin phi sin phi' cos(theta-theta') + cos phi cos phi'
    # distance = rho * arc length
     
    cos = (math.sin(phi1)*math.sin(phi2)*math.cos(theta1 - theta2) + 
           math.cos(phi1)*math.cos(phi2))
    arc = math.acos( cos )
 
    # Remember to multiply arc by the radius of the earth 
    # in your favorite set of units to get length.
    return arc

listSearched = []
myLatitude = 37.0877352
myLongitude = -76.4751584

rEarth = 3963
closestStoreID = 0
closestDistance = 1000
closestQuantity = 0
closestLink = ""

for id in range(35, 376):
    if id in listSearched:
        continue
    print "============================================="
    url = "https://www.abc.virginia.gov/api/stores/inventory/%i/%s" % (id, productID)
    print url

    response = urllib.urlopen(url);
    try:
        data = json.loads(response.read())
    except ValueError:
        continue

    storeID = data["products"][0]["storeInfo"]["storeId"]
    quantity = data["products"][0]["storeInfo"]["quantity"]
    thisLatitude = data["products"][0]["storeInfo"]["latitude"]
    thisLongitude = data["products"][0]["storeInfo"]["longitude"]
    if int(quantity) > 0:
        distance = distance_on_unit_sphere(myLatitude, myLongitude, thisLatitude, thisLongitude) * rEarth
        print "Store ID: %i Quantity: %i Distance: %f" % (int(storeID), int(quantity), distance)
        print "https://www.google.com/maps/place/%f,%f/" % (thisLatitude, thisLongitude)
        if distance < closestDistance:
            closestStoreID = int(storeID)
            closestDistance = distance
            closestQuantity = int(quantity)
            closestLink = "https://www.google.com/maps/place/%f,%f/" % (thisLatitude, thisLongitude)

    listSearched.append(int(storeID))

    for thisStore in data["products"][0]["nearbyStores"]:   
        info = re.findall(r"[-+]?\d*\.*\d+", str(thisStore))
        if int(info[4]) > 0:
            latitude = float(info[0])
            longitude = float(info[3])
            distance = distance_on_unit_sphere(myLatitude, myLongitude, latitude, longitude) * rEarth
            print "Store ID: %i Quantity: %i Distance: %f" % (int(info[2]), int(info[4]), distance)
            print "https://www.google.com/maps/place/%f,%f/" % (latitude, longitude)
            if distance < closestDistance:
                closestStoreID = int(info[2])
                closestDistance = distance
                closestQuantity = int(info[4])
                closestLink = "https://www.google.com/maps/place/%f,%f/" % (latitude, longitude)
        listSearched.append(int(info[2]))

# Now to print out the closest result

print "#########################################"
print "The closest bottle is %f miles away" % closestDistance
print "StoreID %i, Quantity %i" % (closestStoreID, closestQuantity)
print closestLink
